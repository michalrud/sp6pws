---
title: "Intercontest KF"
date: 2021-12-05T09:07:59+01:00
draft: false
---

Dziś dotarła do nas bardzo miła niespodzianka.



Wystarczył jeden dobry start w CQWW aby zostać sklasyfikowanym na 2 miejscu współzawodnictwa organizowanego przez SPDX Club. Intercontest KF ma bardzo długą tradycję sięgającą roku 1972, kiedy to odbyła się pierwsza edycja tych swoistych zawodów.

Jest nam niezmiernie miło, że doceniono nasze starania. Jednocześnie jest to dodatkowa motywacja, aby wciąż doskonalić się w sztuce oepratorskiej i startować w kolejnych zawodach - zarówno tych dużych jak CQWW i SPDX, oraz w tych mniejszych.