---
title: "Harmonogram spotkań"
---

Spotkania klubowe odbywają się co drugi tydzień w soboty od godziny 15:00 (czasu lokalnego). 

Dodatkowe terminy spotkań, przesunięcia będą umieszczane na bieżąco w miarę jak wyklaruje się kalendarz startów w zawodach oraz wyjazdów na plenerowe aktywacje, spotkania krótkofalarskie. 

Zapraszamy de SP6PWS

Wstępny harmonogram spotkań w 2021 roku.

 
 
| Miesiąc     |     Dzień                | Dzień                          |
| ----------- | ------------------------ | ------------------------------ |
| Styczeń     |     16 (sobota)          |      30 (sobota) |
| Luty        |    13 (sobota) ODWOŁANE  |     28 (niedziela) ODWOŁANE |
| Marzec      |  13 (sobota)             |   28 (niedziela) ODWOŁANE |
| Kwiecień    |    10 (sobota)           |     25 (niedziela) |
| Maj         |     8 (sobota)           |  23 (niedziela) |
| Czerwiec    |    5 (sobota)            | 20 (niedziela) ODWOŁANE |
| Lipiec      |  3, 31 (sobota) ODWOŁANE |   18 (niedziela) |
| Sierpień    |    28 (sobota)           |     15 (niedziela) |
| Wrzesień    |    11 (sobota)           |     25 (sobota) |
| Październik |     9 (sobota)           |  24(niedziela) - g.15:00 |
| Listopad    |    6 (sobota)            | 20 (sobota) |
| Grudzień    |    4 (sobota)            | 18 (sobota) |
